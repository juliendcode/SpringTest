package com.wildcodeschool.example.album.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.wildcodeschool.example.album.entities.Album;





@Repository
public interface AlbumDao extends JpaRepository<Album, Long> {
}
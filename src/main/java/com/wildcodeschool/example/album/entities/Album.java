package com.wildcodeschool.example.album.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Album {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		
		private String recordName;
		private String artistName;
		private String recorderName;
		
		@Override
	    public String toString() {
	        return "Record [id=" + id + ", recordName=" + recordName + 
	                ", artistName=" + artistName + ", recorderName=" + recorderName + "]";
		}
	        
        public Album() {
        }

        public Album(String recordName, String artistName, String recorderName) {
            this.recordName = recordName;
            this.artistName = artistName;
            this.recorderName = recorderName;
        }

        public Long getId() {
            return id;
        }    
        
        // On ne définit pas de setId() car l'id sera généré automatiquement */

        public String getRecordName() {
            return recordName;
        }

        public void setRecordName(String recordName) {
            this.recordName = recordName;
        }

        public String getArtistName() {
            return artistName;
        }

        public void setArtistName(String artistName) {
            this.artistName = artistName;
        }

        public String getRecorderName() {
            return recorderName;
        }

        public void setRecorderName(String recorderName) {
            this.recorderName = recorderName;
        }    
    }
	        
	            


package com.wildcodeschool.example.album.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.wildcodeschool.example.album.entities.Album;
import com.wildcodeschool.example.album.repositories.AlbumDao;


@Component
public class OutPutter implements CommandLineRunner {

    private Logger LOG = LoggerFactory.getLogger("Wilder");

    @Autowired
    private AlbumDao albumDao;

    @Override
    public void run(String... args) throws Exception {

        // Checke combien d'objets se trouvent dans la BDD        
        LOG.info("******************");
        LOG.info("Album dans la DB : " + albumDao.count());

        // Crée un nouvel utilisateur et l'enregistre dans la BDD
        Album user1 = new Album("Evil Empire", "RATM", "Andy Wallace");
        LOG.info("******************");
        LOG.info(user1 + " a été crée !");
        albumDao.save(user1);
        LOG.info(user1 + " a été sauvé !");

        // Crée un second utilisateur et l'enregistre dans la BDD
        Album user2 = new Album("Grace", "Jeff Buckley", "Andy Wallace");
        LOG.info("******************");
        LOG.info(user2 + " a été crée !");
        albumDao.save(user2);
        LOG.info(user2 + " a été sauvé !");

        // Lit les informations correspondant au second utilisateur
        Album tempUser = albumDao.findById(2L).get();
        
        /* On écrit "2L" car 
           le type de l'id est Long */
        
        LOG.info("******************");
        LOG.info("nom de l'album " + tempUser.getRecordName());
        LOG.info("nom de l'artiste " + tempUser.getArtistName());
        LOG.info("ingénieur du son " + tempUser.getRecorderName());

        // Liste les utilisateurs enregistrés dans la BDD
        LOG.info("******************");
        LOG.info("albums de la liste ");
        for(Album myUser : albumDao.findAll()) {
            LOG.info(myUser.toString());
        };

        // Supprime le second utilisateur de la BDD
        albumDao.deleteById(2L); 
        
        /* risque de provoquer une erreur si 
           tu n'as pas vidé ta table avant de 
           ton application ! */

        
        /*     Liste les utilisateurs enregistrés dans la BDD
             (permet de vérifier que le second utilisateur
             a bien été supprimé de la BDD) */
        
        LOG.info("******************");
        LOG.info("albums de la liste ");
        for(Album myUser : albumDao.findAll()) {
            LOG.info(myUser.toString());
        };
        
        
        
         /*  UPDATE */
        LOG.info("update du nom de l'artiste");
        user1.setArtistName("Rage Against The machine");
        albumDao.save(user1);
        
        Album tempUser2 = albumDao.findById(1L).get();
        LOG.info("******************");

        LOG.info("nom de l'album " + tempUser2.getRecordName());
        LOG.info("nom de l'artiste " + tempUser2.getArtistName());
        LOG.info("ingénieur du son " + tempUser2.getRecorderName());

        user1.setArtistName("Rage Against The machine");
        albumDao.save(user1);
    }    
}

